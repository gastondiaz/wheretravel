# WhereTravel

[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

WhereTravel es un Sistema de Gestion de Hoteles.

## Configuracion

1° Ejecutar composer update para instalar las dependencias.
##
2° Crear el archivo .env en la raiz del proyecto, el mismo es donde se configurara el acceso a la Base de Datos y el nombre de la misma.
##
3° Ejecutar en cosola php artisan migrate.
 ##
    Esto crear las tablas con sus relaciones
    

## Desarrollo

Gaston Esteban Diaz

##Contacto
 
Email: gastonestebandiaz@gmail.com
##
Skype: gastonestebandiaz


