<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Domicilio extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('domicilio', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('calle');
            $table->integer('numero');
            $table->integer('id_provincia')->unsigned();
            $table->double('latitud');
            $table->double('longitud');
            $table->timestamps();
            $table->foreign('id_provincia')->references('id')->on('provincias');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('domicilio');
    }
}
