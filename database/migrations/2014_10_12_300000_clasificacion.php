<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Clasificacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clasificacion', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('denominacion');
            $table->time('fechaAlta');
            $table->time('fechaBaja');
            $table->integer('id_clasificacion')->unsigned();
            $table->timestamps();
            $table->foreign('id_clasificacion')->references('id')->on('clasificacion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('clasificacion');
    }
}
