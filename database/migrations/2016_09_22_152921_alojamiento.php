<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Alojamiento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alojamiento', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('denominacion');
            $table->boolean('destacada');
            $table->string('descripcion');
            $table->integer('id_domicilio')->unsigned();
            $table->integer('id_clasificacion')->unsigned();
            $table->timestamps();
            $table->foreign('id_domicilio')->references('id')->on('domicilio');
            $table->foreign('id_clasificacion')->references('id')->on('clasificacion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('alojamiento');
    }
}
