<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Habitacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('habitacion', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('denominacion');
            $table->string('codigo');
            $table->string('tipoCama');
            $table->time('horaIngreso');
            $table->time('horaSalida');
            $table->double('tarifa');
            $table->integer('id_alojamiento')->unsigned();
            $table->timestamps();
            $table->foreign('id_alojamiento')->references('id')->on('alojamiento');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('habitacion');
    }
}
