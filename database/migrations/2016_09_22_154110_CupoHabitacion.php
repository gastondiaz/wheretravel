<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CupoHabitacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cupohabitacion', function(Blueprint $table)
        {
            $table->increments('id');
            $table->date('fecha');
            $table->boolean('estado');
            $table->integer('id_habitacion')->unsigned();
            $table->timestamps();
            $table->foreign('id_habitacion')->references('id')->on('habitacion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cupohabitacion');
    }
}
