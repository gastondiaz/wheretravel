<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HabitacionCaracterisitca extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('habitacion_caracterisitca', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('id_habitacion')->unsigned();
            $table->integer('id_caracterisitica')->unsigned();
            $table->timestamps();
            $table->foreign('id_habitacion')->references('id')->on('habitacion');
            $table->foreign('id_caracterisitica')->references('id')->on('caracteristica');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('habitacion_caracterisitca');
    }
}
