<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Foto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('foto', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('rutaArchivo');
            $table->integer('id_alojamiento')->unsigned();
            $table->timestamps();
            $table->foreign('id_alojamiento')->references('id')->on('alojamiento');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('foto');
    }
}
